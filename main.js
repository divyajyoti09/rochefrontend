import users from './users.js'

export class DeveloperDetails {
  constructor() {
    let userNames = users.userNames();
    this.getUsers(userNames);
  }
 
 async getUsers(names) {
   let jobs = [];
 
   for(let name of names) {
     let fetchStars = fetch(`https://api.github.com/users/${name}/repos`).then(
       successResponse => {
         if (successResponse.status != 200) {
           return null;
         } else {
           return successResponse.json();
         }
       },
       failResponse => {
         return null;
       }
     );
     jobs.push(fetchStars);
 
     let fetchProjects = fetch(`https://api.github.com/users/${name}`).then(
       successResponse => {
         if (successResponse.status != 200) {
           return null;
         } else {
           return successResponse.json();
         }
       },
       failResponse => {
         return null;
       }
     );
     jobs.push(fetchProjects);
   }
   
   let results = await Promise.all(jobs);
   let arr =[];
   let m1 = new Map();
   let m2 = new Map();
   let m3 = new Map();
   console.log(results);
   
   for(let i=0;i<results.length;i++) {
     let sum =0;
     let ob = {};
     if(results[i].name) {
     ob.name = results[i].name;
     ob.projects = results[i].public_repos;
     m1.set(results[i].login,results[i].name);
     m2.set(results[i].login,results[i].public_repos);
     } else {
     let innerArray = results[i];
     for(let j=0;j<innerArray.length;j++){
       sum = sum + innerArray[j].watchers_count + innerArray[j].stargazers_count;
       ob.login = innerArray[j].owner.login;
     }
   }
     m3.set(ob.login, sum);
   }
   let table = document.querySelector("table");
   while(table.hasChildNodes())
   {
      table.removeChild(table.firstChild);
   }
 
   this.generateTableHead(table, ["USER NAME:","PROJECT COUNT:","STARS:"]);
   this.generateTable(table, names, m1, m2, m3); 
 }

  generateTableHead(table, data) {
   let thead = table.createTHead();
   let row = thead.insertRow();
   for (let key of data) {
     let th = document.createElement("th");
     let text = document.createTextNode(key);
     th.appendChild(text);
     row.appendChild(th);
   }
 }
 
  generateTable(table, user, names, projects, stars) {
   let thead = table.createTBody();
   for (let i=0 ; i<user.length; i++) {
     let row = thead.insertRow();
     
     let cell1 = row.insertCell();
     let text1 = document.createTextNode(names.get(user[i]));
     cell1.appendChild(text1);
 
     let cell2 = row.insertCell();
     let text2 = document.createTextNode(projects.get(user[i]));
     cell2.appendChild(text2);
     
     let cell3 = row.insertCell();
     let text3 = document.createTextNode(stars.get(user[i]));
     cell3.appendChild(text3);
 
   }
  } 
}
